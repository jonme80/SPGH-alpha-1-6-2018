import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { AppLoading, Asset, Font } from 'expo';
import RootNavigation from './src/navigation/RootNavigation';
import {
  Ionicons,
  MaterialIcons
} from '@expo/vector-icons';

import { Provider } from 'react-redux';
import { createStore } from 'redux';

import AppReducer from './src/reducers';
import AppWithNavigationState from './src/navigation/AppNavigator';
import { AppRegistry } from 'react-native';
import configureStore from './src/stores/configureStore';
import * as actions from './src/actions';
import SortOptions from './src/constants/SortOptions';

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

function cacheFonts(fonts) {
  return fonts.map(font => Font.loadAsync(font));
}

class App extends React.Component {
  store = configureStore();
  // store = createStore(AppReducer);

  state = {
    assetsAreLoaded: false,
  };

  currentSort = SortOptions[0];

  componentWillMount() {
    this.store.dispatch(actions.setSort(this.currentSort));
    this._loadAssetsAsync();
  }

  render() {
    if (!this.state.assetsAreLoaded && !this.props.skipLoadingScreen) {
      return <AppLoading />;
    } else {
      return (
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          {Platform.OS === 'android' &&
            <View style={styles.statusBarUnderlay} />}
          <Provider store={this.store}>
            <AppWithNavigationState />
          </Provider>
        </View>
      );
    }
  }

  async _loadAssetsAsync() {
    const imageAssets = cacheImages([
      require('./assets/images/plates/starter.png'),
      require('./assets/images/plates/bronze.png'),
      require('./assets/images/plates/silver.png'),
      require('./assets/images/plates/gold.png'),
      require('./assets/images/plates/platinum.png'),
      require('./assets/icons/spr-logo-250px.png'),
      require('./assets/icons/spr-logo-500px.png'),
    ]);

    const fontAssets = cacheFonts([
      Ionicons.font,
      MaterialIcons.font,
      { latoRegular: require('./assets/fonts/Lato-Regular.ttf') },
      { montserratRegular: require('./assets/fonts/Montserrat-Regular.ttf')},
    ]);


    try {
      await Promise.all([...imageAssets, ...fontAssets]);
    } catch (e) {
      // In this case, you might want to report the error to your error
      // reporting service, for example Sentry
      console.warn(
        'There was an error caching assets (see: App.js), perhaps due to a ' +
        'network timeout, so we skipped caching. Reload the app to try again.'
      );
      console.log(e);
    } finally {
      this.setState({ assetsAreLoaded: true });
    }
  }
}

AppRegistry.registerComponent('Sustainable PGH Restuarnt', () => App);

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  statusBarUnderlay: {
    height: 24,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
});

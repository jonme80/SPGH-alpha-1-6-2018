import * as actionTypes from '../constants/actionTypes';
import { NavigationActions } from 'react-navigation'

export function setSort(sort) {
    return {
        type: actionTypes.SORT_SET,
        sort,
    };
};

export function sortAndNavigate(sort, route) {
    return function (dispatch, getState) {
        dispatch(setSort(sort));

        dispatch(
            NavigationActions.back()
        )
    }
}

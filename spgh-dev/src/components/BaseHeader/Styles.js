import { StyleSheet } from 'react-native';
import {
    APPBAR_HEIGHT,
    STATUSBAR_HEIGHT,
} from '../../constants/HeightSettings';
import Colors from '../../styles/Colors'

export default styles = StyleSheet.create({
    baseHeader: {
        paddingTop: STATUSBAR_HEIGHT,
        height: STATUSBAR_HEIGHT + APPBAR_HEIGHT,
        paddingLeft: 5,
        paddingRight: 5,
        flexDirection: "row",
        backgroundColor: Colors.seafoamBlue.hex,
        alignItems: "center",
        justifyContent: "space-between",
    },
    headerTitle: {
        fontSize: 16,
        color: Colors.white,
    },
});
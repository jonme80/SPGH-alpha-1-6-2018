import React from 'react';
import {
    Image,
} from 'react-native';


import {
    PropTypes
} from 'prop-types';

import styles from './Styles';

const plateMappings = {
    1: require('../../../assets/images/plates/starter.png'),
    2: require('../../../assets/images/plates/bronze.png'),
    3: require('../../../assets/images/plates/silver.png'),
    4: require('../../../assets/images/plates/gold.png'),
    5: require('../../../assets/images/plates/platinum.png'),
}
const convertToPlateName = (designation) => {
    plateName = 'Starter';

    switch(designation){
        case 1:
            plateName = 'Starter';
            break;
        
        case 2:
            plateName = 'Bronze';
            break;
        
        case 3:
            plateName = 'Silver';
            break;
        
        case 4:
            plateName = 'Gold';
            break;
        
        case 5:
            plateName = 'Platinum';
            break;
        
        default:
            plateName = 'Starter';
            break;
    }

    return plateName;
}

const DesignationPlate = ({ designation, style }) => (
    <Image
        style={[styles.designationPlate, style, ]}
        source={plateMappings[designation]}
    />
);

DesignationPlate.propTypes = {
    designation: PropTypes.number.isRequired,
};

export default DesignationPlate;

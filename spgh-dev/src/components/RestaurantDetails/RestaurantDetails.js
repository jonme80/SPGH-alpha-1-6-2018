import React from 'react';
import {
    View
} from 'react-native';

import {
    PropTypes
} from 'prop-types';

import RestaurantDetailSection from '../RestaurantDetailSection';
import PlatformMapsUri from '../../utilities/PlatformMapsUri'; 


const RestaurantDetails = ({ restaurant }) => (
    <View style={{paddingLeft: 10, }}>
        <RestaurantDetailSection
            content={restaurant.phone}
            label={'Phone'}
            link={`tel:${restaurant.phone}`}
        />
        <RestaurantDetailSection
            content={restaurant.website}
            label={'Website'}
            link={restaurant.website}
        />
        <RestaurantDetailSection
            content={`${restaurant.address},\n${restaurant.city} ${restaurant.state}, ${restaurant.zipcode}`}
            label={'Address'}
            link={PlatformMapsUri(`${restaurant.address}, ${restaurant.city} ${restaurant.state}, ${restaurant.zipcode}`)}
        />
        <RestaurantDetailSection
            content={restaurant.email}
            label={'Email'}
            link={`mailto:${restaurant.email}?subject=Sustianability Question`}
        />
        <RestaurantDetailSection
            content={`${restaurant.score.total}`}
            label={'Sustainablility Score'}
        />
    </View>
)

RestaurantDetails.propTypes = {
    restaurant: PropTypes.shape({
    }),
};

export default RestaurantDetails;
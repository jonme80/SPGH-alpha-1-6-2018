import { StyleSheet } from 'react-native';
 
export default styles = StyleSheet.create({
    restaurantTitle: {
        backgroundColor: 'transparent',
        color: '#fff',
        fontSize: 20,
    },
    overlay: {
        height: 50,
        justifyContent: 'flex-start',
        paddingLeft: 16,
        paddingTop: 0,
    }
});
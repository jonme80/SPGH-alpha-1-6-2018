import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
    displayedImage: {
        width: null,
        height: 200,
        justifyContent: 'flex-end',
        flexDirection: 'column',
    },
    loadingStyle: {
        height: "100%",
        width: "100%",
        backgroundColor: "#649b9b",
    }
});

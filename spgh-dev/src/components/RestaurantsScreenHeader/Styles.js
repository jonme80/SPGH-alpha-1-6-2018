import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
    searchText: {
        fontSize: 16,
        color: "#fff",
        flex: 1,
    },
    searchBar: {
        paddingLeft: 15,
        paddingRight: 15,
        height: 35,
        flex: 1,
        backgroundColor: "#649b9b",
        borderRadius: 5,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
    },
    navigationLogo: {
        width: 50,
        height: 50,
    },
    navigationButton: {
        color: "#fff",
    }
});

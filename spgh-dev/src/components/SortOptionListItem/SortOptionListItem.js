import React from 'react';

import {
    TouchableHighlight,
    View,
} from 'react-native';
import { connect } from 'react-redux';
import * as actions from '../../actions';

import { LatoText } from '../StyledText';
import Colors from '../../styles/Colors';
import Styles from './Styles';


const SortOptionListItem = ({ sortOption, setSort }) => (
    <TouchableHighlight
        onPress={() => setSort(sortOption)}
        underlayColor={Colors.gold.hex}
    >
        <View style={Styles.listItem}>
            <LatoText>
                {sortOption.title}
            </LatoText>
        </View>
    </TouchableHighlight>
)

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
    setSort: (selectedSort) => {
        dispatch(actions.sortAndNavigate(selectedSort, 'RestaurantList'));
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(SortOptionListItem);

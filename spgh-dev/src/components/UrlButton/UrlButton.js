import React from 'react';
import PropTypes from 'prop-types';
import {
    Linking,
    TouchableOpacity,
    View,
    Text,
    StyleSheet
} from 'react-native';
import { LatoText } from '../StyledText';
import styles from './Styles';

class UrlButton extends React.Component {
  static propTypes = {
    url: PropTypes.string,
  };

  handleClick = () => {
    Linking.canOpenURL(this.props.uri).then(supported => {
      if (supported) {
        Linking.openURL(this.props.uri);
      } else {
        console.log('Don\'t know how to open URI: ' + this.props.uri);
      }
    });
  };

  render() {
    return (
      <TouchableOpacity
        onPress={this.handleClick}>
        <View>
          <LatoText style={styles.linkText}>{this.props.label || `Open ${this.props.uri}`}</LatoText>
        </View>
      </TouchableOpacity>
    );
  }
};

UrlButton.propTypes = {
    url: PropTypes.string,
    label: PropTypes.string,
};

export default UrlButton;
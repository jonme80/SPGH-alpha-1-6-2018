import { Platform } from 'react-native';

const TITLE_OFFSET = Platform.OS === 'ios' ? 70 : 56;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 52 : 64;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0;

module.exports = {
    APPBAR_HEIGHT,
    STATUSBAR_HEIGHT,
    TITLE_OFFSET,
};

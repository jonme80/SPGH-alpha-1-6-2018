const sortOptions = [
    {
        order: 'asc',
        title: 'Name - A to Z',
        key: 'name',
        id: 0,
    },
    {
        order: 'desc',
        title: 'Name - Z to A',
        key: 'name',        
        id: 1,
    },
    {
        order: 'desc',
        title: 'Plate Designation - Platinum to Starter',
        key: 'sustainabilityScore',
        id: 2,
    },
    {
        order: 'asc',
        title: 'Plate Designation - Starter to Platinum',
        key: 'sustainabilityScore',        
        id: 3,
    }
];

export default sortOptions;

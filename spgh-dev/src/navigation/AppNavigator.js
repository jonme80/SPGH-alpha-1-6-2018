import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { addNavigationHelpers, StackNavigator } from 'react-navigation';

import RestaurantNavigation from './RestaurantNavigation';
import SortScreen from '../screens/SortScreen';
import RestaurantsScreen from '../screens/RestaurantsScreen';
import RestaurantScreen from '../screens/RestaurantScreen';
import AboutScreen from '../screens/AboutScreen';

export const AppNavigator = StackNavigator(
  {
    Home: { screen: RestaurantNavigation },
    Sort: { screen: SortScreen, },
	About: { screen: AboutScreen, },
  },
  {
    headerMode: 'none',
    mode: 'modal',
  }
);


const AppWithNavigationState = ({ dispatch, nav }) => (
  <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />
);

AppWithNavigationState.propTypes = {
  dispatch: PropTypes.func.isRequired,
  nav: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  nav: state.nav,
});

export default connect(mapStateToProps)(AppWithNavigationState);
import { combineReducers } from 'redux';
import { NavigationActions } from 'react-navigation';

import { AppNavigator } from '../navigation/AppNavigator';
import sort from './sort';
import RouteNames from '../constants/RouteNames';

let initialNavState = AppNavigator.router.getStateForAction(
  NavigationActions.init()
);

function nav(state = initialNavState, action) {
    let nextState;
    switch (action.type) {
        case RouteNames.RestaurantList:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: RouteNames.RestaurantList, }),
                state
            );
            break;
        case RouteNames.RestaurantDetails:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: RouteNames.RestaurantDetails, }),
                state
            );
            break;
		case RouteNames.About:
            nextState = AppNavigator.router.getStateForAction(
                NavigationActions.navigate({ routeName: RouteNames.About, }),
                state
            );
            break;
        default:
            nextState = AppNavigator.router.getStateForAction(action, state);
            break;
    }

    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
}

const AppReducer = combineReducers({
    nav,
    sort,
});

export default AppReducer;
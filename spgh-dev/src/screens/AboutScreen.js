import React from 'react';
import { View, Image, TouchableOpacity, Linking } from 'react-native';
import { LatoText } from '../components/StyledText';
import RestaurantScreenHeader from '../components/RestaurantScreenHeader';

class AboutScreen extends React.Component
{
	static navigationOptions = ({ navigation }) => ({});
	
	render()
	{
		return (
			<View style={{ flex: 1, backgroundColor: '#ffffff' }}>
				<RestaurantScreenHeader
				  navigation={this.props.navigation}
				  style={{ flex: .25, }}
				/>
				<View style={{ flex: 10, alignItems: 'center', paddingBottom: 17.5, }}>
					<Image
						source={require('../../assets/icons/spr-logo-500px.png')}
						style={{ resizeMode: 'contain', flex: 5 }}
					/>
					<Image
						source={require('../../assets/images/about/checkerboard.png')}
						style={{ resizeMode: 'contain', flex: 1}}
					/>
				</View>
				<View style={{ flex: 10, alignItems: 'center' }}><LatoText style={{ textAlign: 'center' }}>
					A Sustainable Pittsburgh Restaurant has made the commitment to taking its operational effects into account where the environment, economy, and social equity are concerned. It aims to enhance the lives of its guests, workers, and people in its community, minimize its environmental impacts, and understand the economic value in doing these things to better itself, its industry, and our regional economy. It is part of a community of restaurants invested in positive change for the future of southwestern Pennsylvania. 
				</LatoText></View>
				<View style={{ flex: 2, padding: 10, flexDirection: 'row',  alignItems: 'center', justifyContent: 'center', }}>
					<View style = {{ padding: 10 }}>
						<TouchableOpacity onPress={() => Linking.openURL('https://twitter.com/dine_sustainpgh')}>
							<Image
								source={require('../../assets/images/social_media/twitter.png')}
								style = {{ resizeMode: 'stretch'}}
							/>
						</TouchableOpacity>
					</View>
					<View style = {{ padding: 10 }}>
						<TouchableOpacity onPress={() => Linking.openURL('https://www.facebook.com/sustainablepghrestaurants?ref=aymt_homepage_panel')}>
							<Image
								source={require('../../assets/images/social_media/facebook.png')}
								style = {{ resizeMode: 'stretch', }}
							/>
						</TouchableOpacity>
					</View>
					<View style = {{ padding: 10 }}>
						<TouchableOpacity onPress={() => Linking.openURL('https://www.instagram.com/sustainable_pgh_restaurant/')}>
							<Image
								source={require('../../assets/images/social_media/instagram.png')}
								style = {{ resizeMode: 'stretch', }}
							/>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}
}

export default AboutScreen;
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';
import {batchActions, enableBatching} from 'redux-batched-actions';
import rootReducer from '../reducers/index';

const logger = createLogger();

const createStoreWithMiddleware = applyMiddleware(
  logger,
  thunk,
)(createStore);

export default function configureStore(initialState) {
  return createStoreWithMiddleware(enableBatching(rootReducer), initialState);
}